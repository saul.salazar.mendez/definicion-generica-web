# Definición genérica de una aplicación web vue

El propósito de este proyecto es definir los componentes de una aplicación web. Esto con el fin de crear librerías que consuman frameworks css y sea fácil e cambiar de un tema a otro.

## Contenedor principal de la aplicación

Una aplicación genérica vue usara **vue-router** y contara con **notificaciones**.
Tomando en cuenta eso tenemos que el contenedor principal de la aplicación es:
```html
<div id="app">
    <!-- Area de notificaciones-->
    <notificacion></notificacion>
    <!-- Area en la que desplegara las vistas-->
    <router-view></router-view>
</div>
```

## Notificaciones
Las notificaciones son mensajes que aparecen sobre toda la web, indican si hay algun tipo de error, warning, información o aviso de que sucedió algo.

![alt text](notificacion.png "Notificacion")

Las notificaciones trabajan con la libreria de notificaciones-js, en esta libreria se hacen push de notifiaciones.
Se puede indicar de manera opcional el tiempo que durara la notificación y la ruta que se accederá cuando se de click la ruta. Si no se indica el tiempo por default sera de 1000 milisegundos
```javascript
addNotificacion(tipo,titulo, mensaje, tiempo = 1000, ruta = null
//Notificacion de información
addNotificacion('info', 'Titulo', 'Esta es una notificacion')
//Notificacion de que sucedió algo
addNotificacion('success', 'Titulo', 'Esta es una notificacion')
//Notificacion de error
addNotificacion('danger', 'Titulo', 'Esta es una notificacion')
//Notificacion de warning
addNotificacion('warning', 'Titulo', 'Esta es una notificacion')
```
## Vistas
Una vista es como se ve la pagina web de manera completa. Esta contendra una barra de titulo y un menu, el contenido de la pagina y un footer.(ver imagen)

![alt text](vista.png "Vista")

Esta se desplegara el componete **router-view**

Teniendo esto en cuenta la vista esta definida como
```html
<vista>
    <template v-slot:menu>
        <menu-app></menu-app>
    </template>
    <contenido></contenido>
    <template v-slot:footer>
        <footer-app></footer-app>
    </template> 
</vista>
```

## Menú de la aplicación
Este componente contendra el titulo y el menu de la app.

Ejemplo de menu boostrap:

![alt text](Menu-boostrap.png "Menu")

Menu de la app
```html
<menu-app :menu="menu" class="color-menu"></menu-app>
```
Donde el menu tiene la siguiente estructura:
```javascript
menu = {
    titulo: ' Navbar',
    links: [
        {
            label: 'Home',
            url: '/home'
        }
    ]
}
```
## Contenido
El contenido de la vista este contiene de manera opcional un breadcrumd y el contenido en si.

![alt text](contenido.png "Menu")

Contenido
```html
<contenido>
    <breadcrumd :rutas="rutas"></breadcrumd>
    <div>Contenido</div>
</contenido>
```
## Breadcrumd
El breadcrum esta definido por una arreglo de rutas, donde la ultima ruta indica la vista en la que se encuentra. Sirve para navegar dentro de la app.
breadcrumd
```html
<breadcrumd :rutas="rutas"></breadcrumd>
```
Donde el arreglo rutas esta definido como
```javascript
rutas = [
    {
        label: 'Inicio',
        url: 'ruta'
    }
]
```
## Formulario

Los formularios es una colección de contenedores con inputs en su interior.
Los contenedores pueden ser paneles, tabs y acordeones.


## Inputs

Los inputs en un conjunto de etiqueta y entrada que se captura o edita.
La configuracion general de un input es:
```html
<input v-model="item">    
</input>
```
Donde el item tiene la siguiente forma
```javascript
item = [
    {
        label: 'Label',
        valor: 'valor',
        reglas: [] /* Ver el apartado de reglas*/
    }
]
```
Tipos de input
- Input text
- Input numero
- Input date
- Input time
- Input autocomplete
- Input select //falta definir
- Input password
- Input textarea
- Input file //falta definir
- Input files //falta definir

### Input text
Se usa para editar cadenas de texto.
```html
<input-text v-model="item">    
</input-text>
```
### Input numero
Se usa para editar valores numéricos.
```html
<input-numero v-model="item">    
</input-numero>
```
### Input date
Se usa para editar fechas.
```html
<input-date v-model="item">    
</input-date>
```
### Input time
Se usa para editar horas.
```html
<input-time v-model="item">    
</input-time>
```
### Input autocomplete
Se usa para editar cadenas de texto, con la posibilidad de autocompletar.
```html
<input-autocomplete v-model="item" :opciones="opciones">    
</input-autocomplete>
```
Donde opciones es un arreglo de texto con las opciones de autocompletado.
```javascript
const opciones = ['opcion 1', 'opcion 2'];
```
### Input password
Se usa para editar cadenas de texto.
```html
<input-password v-model="item">    
</input-password>
```
### Input textarea
Se usa para editar cadenas de texto de gran longitud.
```html
<input-textarea v-model="item">    
</input-textarea>
```
## Reglas
Las reglas de validación de campos. Sirven tanto para definir campos de tablas de base de datos como para definir campos de formularios.
Estas reglas complementan la definición de los formularios  y estarán definidas en un proyecto aparte que se téndra que importar para poderse usar.
Las reglas pueden ser la siguientes:
- **requerido**: Hace el campo requerido
- **max_value**: Indica el valor máximo permitido. Aplica a valore numéricos y fechas.
- **min_value**: Indica el valor mínimo permitido. Aplica a valore numéricos y fechas.
- **max_len**: Indica la longitud máxima de texto.
- **min_len**: Indica la longitud mínima de texto.
- **regex**: Indica un regex personalizado para validar.

En javascript se definirán las siguientes clases para este proposito:
Requerido, MaxValue, MinValue, MaxLen, MinLen, Regex

```javascript
New Requerido('Mensaje de alerta');
New MaxValue(10, 'Mensaje de alerta'); 
New MinValue(10, 'Mensaje de alerta');
New MaxLen(10, 'Mensaje de alerta');
New MinLen(10, 'Mensaje de alerta');
```
**Nota**: si se omite el Mensaje de alerta se usara el por default.

